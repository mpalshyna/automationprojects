import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class FreshMarket {
    private WebDriver driver;
    WebDriverWait wait;

    @BeforeTest
    public void setupTest() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 15);
    }

    @AfterTest
    public void teardown() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Test
    public void test() {
        driver.get("https://pmx-qa6-cs.saas-n.com/px/login");
        String USERNAME_XPATH = "//input[@name='username']";
        WebElement username = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(USERNAME_XPATH)));
        username.sendKeys("MPALSHYNA_QA6_I@paymode.com");

        String PASSWORD_XPATH = "//input[@name='password']";
        WebElement password = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PASSWORD_XPATH)));
        password.sendKeys("test1QAi234");

        String LOGIN_XPATH = "//button[text()='Login']";
        WebElement login = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(LOGIN_XPATH)));
        login.click();

        wait.until(ExpectedConditions.invisibilityOf(login));

        if (driver.getTitle().contains("Verify Your Account")) {

            String RADIO1_XPATH = "//input[@name='phoneNumberDisplay1']";
            WebElement radio1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(RADIO1_XPATH)));
            radio1.click();

            String SUBMIT_XPATH = "//button[@type='submit']";
            WebElement submit = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SUBMIT_XPATH)));
            submit.click();

            wait.until(ExpectedConditions.titleContains("Verify Your Account - Paymode-X"));

            String ACODE_XPATH = "//input[@name='phoneAuthenticationCode']";
            WebElement acode = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(ACODE_XPATH)));
            acode.sendKeys("1111");

            String SUBMITCODE_XPATH = "//button[@type='submit']";
            WebElement submitcode = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SUBMITCODE_XPATH)));
            submitcode.click();
        }

        wait.until(ExpectedConditions.titleContains("Membership Search - Paymode-X"));


        String COMPANY_XPATH = "//input[@name='companyName']";
        WebElement company = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(COMPANY_XPATH)));
        company.sendKeys("fresh market");

        String SEARCH_XPATH = "//button[@type='submit']";
        WebElement search = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SEARCH_XPATH)));
        search.click();

        wait.until(ExpectedConditions.titleContains("Company Information - Membership Administration - Paymode-X"));


        String ADMIN_XPATH = "//span[text() ='Admin']";
        WebElement admin = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(ADMIN_XPATH)));
        admin.click();
        String USERS_XPATH = "//*[@class = 'dropdown-menu']//*[text() = 'Users']";
        WebElement users = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(USERS_XPATH)));
        users.click();

        wait.until(ExpectedConditions.titleContains("Users - Membership Administration - Paymode-X"));

        String ADDUSER_XPATH = "//*[@class='btn btn-primary add-user']";
        WebElement adduser = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(ADDUSER_XPATH)));
        adduser.click();

        wait.until(ExpectedConditions.titleContains("Add New User"));

        String FIRSTNAME_XPATH = "//input[@name='firstname']";
        WebElement firstname = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(FIRSTNAME_XPATH)));
        firstname.sendKeys("Market1");

        String LASTNAME_XPATH = "//input[@name='lastname']";
        WebElement lastname = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(LASTNAME_XPATH)));
        lastname.sendKeys("Fresh1");

        String TITLE_XPATH = "//input[@name='title']";
        WebElement title = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(TITLE_XPATH)));
        title.sendKeys("Executive director");

        String PHONECODE_XPATH = "//select[@id='uphonenocc']";
        WebElement phonecode = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PHONECODE_XPATH)));
        phonecode.click();
        String COUNTRYCODE_XPATH = "//*[@class='contentText']//*[@value='380']";
        WebElement countrycode = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(COUNTRYCODE_XPATH)));
        countrycode.click();

        String MAINPHONE_XPATH = "//input[@name='uphoneno']";
        WebElement mainphone = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(MAINPHONE_XPATH)));
        mainphone.sendKeys("501705851");

        String EMAIL_XPATH = "//input[@name='emailaddress']";
        WebElement email = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(EMAIL_XPATH)));
        email.sendKeys("palshyna@gmail.com");

        String EMAILCONFIRM_XPATH = "//input[@name='confirmemailaddress']";
        WebElement emailconfirm = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(EMAILCONFIRM_XPATH)));
        emailconfirm.sendKeys("palshyna@gmail.com");

        String CBUTTON_XPATH = "//input[@value='Continue >>']";
        WebElement cbutton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CBUTTON_XPATH)));
        cbutton.click();

        String MFA_PUSH_RADIO_XPATH = "//input[@value='OOBSMS']";

        if (!driver.findElements(By.xpath(MFA_PUSH_RADIO_XPATH)).isEmpty()) {

            String RADIO_BUTTON_TEXT_XPATH = "//input[@value='OOBSMS']";
            WebElement radio_button_text = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(RADIO_BUTTON_TEXT_XPATH)));
            radio_button_text.click();

            String AUTHENTICATE_XPATH = "//button[@id='btn-authenticate-now']";
            WebElement authenticate = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(AUTHENTICATE_XPATH)));
            authenticate.click();

//            wait.until(ExpectedConditions.titleContains("Verify Your Account - Paymode-X"));

            String ENTER_CODE_XPATH = "//input[@id='codeInput']";
            WebElement enter_code = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(ENTER_CODE_XPATH)));
            enter_code.sendKeys("1111");

            String SUBMIT_CODE_BUTTON_XPATH = "//button[@id='btn-submit']";
            WebElement submit_code_button = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SUBMIT_CODE_BUTTON_XPATH)));
            submit_code_button.click();

            String OK_BUTTON_XPATH = "//button[@id='btn-ok']";
            WebElement ok_button = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OK_BUTTON_XPATH)));
            ok_button.click();
        }

        wait.until(ExpectedConditions.titleContains("Paymode-X Account Privileges"));


    }
}


//        WebElement username = driver.findElement(By.xpath("//*[@name='username']"));
//        WebElement username2 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name='username']")));
//
//        wait.until(ExpectedConditions.titleContains("Login Page"));
//
//
//        username.sendKeys("batinternal@saas-p.com");
//        String value = username.getText();
//
//        Assert.assertEquals("string", "string", "Strings must be equal");
//
//
//        driver.findElement(By.name("username"));
//new Select(login).selectByValue("value");

